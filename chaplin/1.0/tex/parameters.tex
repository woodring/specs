\label{sec:PARAMETERS}

A \Current database is a collection of results sampled by a set of visualization parameters. Each parameter is described with an entry in ``parameter\_list'' section of the json file. Parameters will have a a set of \textit{values}, a \textit{default} value from among the values, a \textit{name} and a suggested \textit{label}.

Parameters may also have a suggested widget \textit{type}. The type is used as a hint for viewing applications to inform what type of GUI widget is most appropriate for this control. A type of ``hidden'' should generally not be displayed to the user. A type of ``range'' is best displayed with a menu or scalar bar that lets the user select one value at a time. A type of ``option'' is best displayed with a combobox type widget that lets the user select many values at once.

Parameters may also have a \textit{role}. The roll more fully describes the purpose of the parameter for generation and display purposes. A role of ``layer'' indicates that this parameter defines what specific object or objects should be shown. A role of ``control'' indicates that this parameter is one of a set that controls aspects of a specific object (i.e. filter settings). A role of ``field'' indicates that this parameter represents a set of color inputs.

For parameters with a role of ``field'' the additional annotations ``types'' and ``valueRanges'' give the specific image component type corresponding to each possible value and the minimum and maximum numerical values for rasters with the ``types'' designation of ``value'' image.

Examples of typical parameters in a cinema database are as follows.

\subsubsection{Time}
\label{sec:Time}

Time varying data can be sampled at arbitrary points along the temporal domain.
\begin{verbatim}
 "parameter_list": {
   "time": { "default": "0.000000e+00",
             "values": ["0.000000e+00", "1.000737e-04", "1.999051e-04"],
             "type": "range",
             "label": "time" },
   ...
 }
\end{verbatim}

\subsubsection{Camera Positions}
\label{sec:camera}

In a traditional visualization setting the user can manipulate the camera arbitrarily. For cinema we discretize and organize the range of captured motions into of several camera motion classes.

\begin{verbatim}
 "parameter_list": {
   "phi": { "default": -180,
            "values": [-180, -150, -120, -90, -60, -30,
                       0,
                       30, 60, 90, 120, 150],
            "type": "range",
            "label": "phi" },
   "theta": { "default": -90,
              "values": [-90, -64, -38, -12, 13, 38, 63, 88],
              "type": "range",
              "label": "theta" },
   ...
 }
\end{verbatim}
Here, we have a legacy ``phi-theta'' camera model in which the discrete camera positions are described by the product of two parameters with angular positions.

For finer control, Chaplin provides additional camera models. The specific camera model in use within a database is listed in the ``camera\_model'' entry of the json files metadata section. Recall from section~\ref{sec:grammar} that a camera model is allowed one of four values.
\begin{verbatim}
CAMERA_MODEL ::= "camera_model" : "static"|"phi-theta"|
                               "azimuth-elevation-roll"|"roll-pitch-yaw"
\end{verbatim}

In a \textit{static} camera the position and orientation is fixed an unchanging. Static cameras do not require a corresponding parameter entry.

In a \textit{phi-theta} spherical type camera the camera ``from'' position varies over a set of regularly sampled angular positions centered around a chosen focal point. Phi-Theta cameras will have one or two corresponding parameters entries. In Cinema, phi is defined to be rotations around the vertical axis, going from -180 to 180 degrees, inclusive on the negative only. Theta is defined to be rotations from south to north pole, ranging from -90 to 90 degrees inclusive.

In the more general \textit{pose} based cameras, new to Cinema~\CinemaSpecVersion, we store complete camera reference frames from an arbitrary collection of viewpoints. Pose based camera are allowed to move over time and track objects in the scene.

Pose cameras consist of a parameter named pose, which contains any number of 3x3 normalized camera reference frames. To keep the combination of camera positions over time and view directions manageable, we vary the camera's local coordinate frame consistently at every time step, but offset them from a different location at each time step. Two simple variations on this theme move the coordinate frame about specific from points \textit{yaw-pitch-roll} or outward-facing, and around specific look at points \textit{azimuth-elevation-roll} or inward-facing. Inward facing camera are an improvement upon the earlier phi-theta camera type.

For example a pose based, inward facing, object tracking camera for a dataset with three time steps might contain,
\begin{verbatim}
 "parameter_list": {
  "time": {
    "values": ["0.0e+00",
               "1.0e-04",
               "8.0e-04"],
     ... },
  "pose": {
    "values": [[[-1.0, 1.224e-16, 7.498e-33],
                [0.0, 6.123e-17, -1.0],
                [-1.224e-16, -1.0, -6.123e-17]],
               [[-1.0, 1.109e-16, 5.175e-17],
                [0.0, 0.422, -0.906],
                [-1.224e-16, -0.906, -0.422]],
               ... ],
    ...
    },
  ...
 },
 "metadata": {
   "camera_model": "azimuth-elevation-roll",
   "camera_eye": [[0.0, 0.0, 66.92],  /*corresponds to time 0.0e+00*/
                  [1.0, 0.0, 66.92],  /*corresponds to time 1.0e-04*/
                  [2.0, 0.0, 66.92]], /*corresponds to time 8.0e-04*/
   "camera_at": [[0.0, 0.0, 0.0], /*as above */
                 [1.0, 0.0, 0.0],
                 [2.0, 0.0, 0.0]]
   "camera_up": [[0.0, 1.0, 0.0], /*as above */
                 [0.0, 1.0, 0.0],
                 [0.0, 1.0, 0.0]],
   ...
 },
\end{verbatim}
Besides the entries above additional pose camera metadata includes camera near and far planes and camera field of view settings. The complete set is optional, but when present it allows for the calculation of the exact coordinates of every pixel in the database.

\subsubsection{Objects}
\label{sec:objects}

Different items may be displayed in the scene. A visibility parameter controls which items are visible at any given time. For a composite database the generator will ensure that only one item is visible in any given raster while the images are being produced. The viewer uses the same parameter to allow multiple items to be visible and depth composites multiple rasters together to produce the effect. Visibility parameters have a \textit{role} annotation of ``layer''.


\begin{verbatim}
 "parameter_list": {
   "vis": {
     "values": [
       "Contour1",
       "Wavelet1",
       ...
     ],
     "role": "layer",
     "type": "option",
     ...
   }
 }
\end{verbatim}

\subsubsection{Operators}
\label{sec:operators}

Zero or more operators, such as clipping plane and isocontour samples along with their respective ranges are represented by parameters. Operators, like other parameters are cumulative. For each operator, the value is applied and the resulting scene is recorded. Operator parameters have a \textit{role} annotation of ``control''.

\begin{verbatim}
 "parameter_list": {
    "Contour1": { "values": [37.3531, 97.2221, 157.091, 216.96, 276.829],
                  "role": "control",
                  ... },
    ...
 }
\end{verbatim}

\subsubsection{Color}
\label{sec:colors}

Color parameters produce components for deferred rendering. Color parameters have a \textit{role} annotation of ``field''. In a Cinema workflow, the application producing a Cinema Database creates a set of image constituents for each sample in the parameter space. Viewing applications use these constituents to draw objects and to color them dynamically dependent on the user's choices for solid color or colormapped value arrays. Recall that ``types'' and ``valueRanges'' annotations are important for interpreting the output of each possible value for a color control.

An example of a color specification is as follows:
\begin{verbatim}
 "parameter_list": {
   "colorContour1": { "values": ["depth", "luminance", "RTData_0"],
                      "types": ["depth", "luminance", "value"],
                      "valueRanges": {"RTData_0": [37.3531, 276.829]},
                      "role": "field" },
   ...
  }
\end{verbatim}

Here we have all possibilities for the color constituents of an object named ``Contour1''. The color constituents consist of a depth raster, a luminance raster, and a value raster for a scalar quantity called ``RTData''. The RTData array varies over the entire simulation and for all objects between 37 and 277.

The types entries describe the specific image constituent type. In general the image constituents can be:
\begin{itemize}
\item \textbf{Depth} image. This encodes a depth value for every pixel, relative to the camera. Required for compositing. In \chaplin, depth images are stored as floating point rasters, ranging from 0 for the near plane to 255 for the far plane, and kept in zlib compressed ``.Z'' files. As with value images the word size is 32bit. The endianness and image dimensions are recorded in the metadata section's endian and image\_size entries.
\item \textbf{Luminance} image. This encodes a rendered shading brightness for each pixel. Required for lighting to be included in the final rendering. In \chaplin, luminance images are stored in RGB images where the R component contains the ambient gray value, G contains the diffuse, and B contains the specular component. Of these, currently only the diffuse component is used. All components range from 0 to 255.
\item \textbf{Color} image. This encodes a standard RGB value for each pixel - the result of rendering the viewpoint from the camera. Color images can not be dynamically color mapped.
\item \textbf{Lookup Table} image. This is a Color type image with the connatation that the colors correspond specifically to statically color mapped values as opposed to the more general case which could also represent texture mapped or solid colored renderings.
\item \textbf{Value} image. This encodes an arbitrary array value associated with the data that is visualized at each pixel. Global ranges for each array, i.e. the min and max value for a value over all time steps and parameter settings, are recorded in the meta data file. In \chaplin, value images are stored as floating point rasters, where the value is either normalized between 0.0 and 1.0 or kept unmodified, and stored in a ``.Z'' file. This is described more fully in section~\ref{sec:valuemode}.
\end{itemize}
