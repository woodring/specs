\label{sec:metadata}

In addition to the sampled data as represented by the parameter space and constraint set, a cinema database contains additional data that describes the version of the database, supplemental information about cameras, and additional information about relations ships between objects in the scene.

\subsubsection{Version Information}

Version information allows cinema based applications to allow for backward compatibility.
The \textit{type} entry states whether this cinema store holds a \textit{Simple} non-compositable database, or a compositable database as described in this document. Furthermore a \textit{version} entry state the specific revision level of both types. The \textit{store\_type} entry describes the lower level file format that the raster and other data is kept in. This should be ``FS'' for the reference implementation described in section~\ref{sec:storage} that is based on named files and directories.

\begin{verbatim}
 "metadata": {
   "type": "composite-image-stack",
   "store_type": "FS",
   "version": "0.2",
   ...
 },
\end{verbatim}

\subsubsection{Value Mode}
\label{sec:valuemode}

Another piece of information in the metadata section for composite type databases is the \textit{value\_mode} entry. This is a record of the type for value raster images. When the entry is absent, or contains the value ``1'', it means that the value rasters were made using the approximative method that predates version \chaplin. Prior to \chaplin, value raster images were approximated by scaling numerical quantities to the range of 0 to $2^{24}$, outputting the results into standard RGB image files, and then using the range information for the array to recover numbers that were close to the original values. In \chaplin these value rasters are automatically converted to floating point rasters in which each pixel is a number between 0.0 and 1.0 and then stored in a zlib compressed ``.Z'' file.

If the entry is present and contains the value ``2'' it means that the value rasters were created directly and contain exact numerical quantities instead of normalized ones.

If the entry is present and contains the value ``-1'' it means that the prebaked, unchangeable, color lookuptables are output as RGB rasters instead of the value rasters. In this case their will be only ``lut'' type rasters instead of ``value'' type rasters.

\subsubsection{Camera}

The camera model and camera details sections of the metadata section are used in concert with the time and camera type parameters to specify camera positions. The specific type of camera model for the database is given in the camera\_model entry. For ``azimuth-elevation-roll'' and ``yaw-pitch-roll'' types cameras, base camera positional information, about which the camera moves according to the ``pose'' parameter, is given in the camera\_eye, camera\_at, camera\_up, camera\_nearfar, and camera\_angle entries as described in section~\ref{sec:camera}

\subsubsection{Pipeline}

A pipeline entry directly encodes relationships between objects in the generating scene. It, in combination with the constraints and roles information is helpful for building up GUIs.

Here a graph of objects in the scene is explicitly recorded. This is not entirely redundant with the constraints system described in section~\ref{sec:CONSTRAINTS} because abstractly the later variable centric view is more inclusive and concretely because there is not generally a one to one relationship between objects and parameters. When objects are present the pipeline information facilitates organizing the controllable options to present them to the user.

\begin{verbatim}
 "metadata": {
   "pipeline": [
     { "children": [],
       "parents": [
         "2488"
       ],
       "id": "2687",
       "visibility": 1,
       "name": "Contour1" },
     { "children": [
         "2687"
       ],
       "parents": [
         "0"
       ],
       "id": "2488",
       "visibility": 1,
       "name": "Wavelet1" },
     ...
   ],
   ...,
 }
\end{verbatim}

\subsubsection{Endian and Image Size}

We store depth and value rasters as raw zlib compress 32-bit floating point files. The endian and image\_size entries record the endianness that the floats are stored in and the width and height (in pixels) of the rasters.


\subsection{Name Pattern}
\label{sec:NAMEPATTERN}

The \textit{name\_pattern} entry is a legacy of the simple database format where it is used to fully describe the directory structure of the ``FS'' type database. In the composite database, the constraints and hierarchical nature of the data make it impractical to fully describe the layout with a regular expression. It is used only to specify the default raster file format for RGB type color component rasters. The trailing file extension determines this.
