\label{sec:cinema}

Extreme scale scientific simulations are leading a charge to exascale computation, and data analytics runs the risk of being a bottleneck to scientific discovery. Due to power and I/O constraints, we expect in situ visualization and analysis will be a critical component of these workflows. Options for extreme scale data analysis are often presented as a stark contrast: write large files to disk for interactive, exploratory analysis, or perform in situ analysis to save detailed data about phenomena that a scientists knows about in advance. We present a novel framework for a third option – a highly interactive, image-based approach that promotes exploration of simulation results, and is easily accessed through extensions to widely used open source tools. This in situ approach supports interactive exploration of a wide range of results, while still significantly reducing data movement and storage.

More information about the overall design of Cinema is available in the paper, \textit{An Image-based Approach to Extreme Scale In Situ Visualization and Analysis}\cite{cinemaSC14}.

A Cinema database is a collection of data that supports this image-based approach to interactive data exploration. It is a set of images and associated metadata, and is defined and an example given in the following sections.

We invite public comment on this specification. Please send comments to \texttt{\small cinema-dev@lanl.gov}.

\subsection{Use Cases}
A Cinema Database supports the following three use cases:
\begin{enumerate}
\item Searching/querying of meta-data and samples. Samples can be searched purely on metadata, on image content, on position, on time, or on a combination of all of these.
\item Interactive visualization of sets of samples.
\item Playing interactive visualizations, allowing the user on/off control of elements in the visualization.
\end{enumerate}

This database specification separates the organization of a set of images from the mechanics of how these images are both generated and stored. Any system that can save images while varying a defined set of parameters can produce \textit{Simple} Cinema Databases. Any system that can do the above and produce depth and other component images of isolated objects in the scene can produce \chaplin Cinema Databases. In this document we focus on a particular implementation of the database in which data products are stored as named files in a standard file system with a json file that described the overall structure of the set.

\subsection{What is a Cinema Database?}
A Cinema database is a set of precomputed visualization samples that can be queried and interactively viewed. This document describes release \CinemaSpecVersion of the Cinema \chaplin Database, in which image constituents are stored instead of standalone fully pre-rendered images. The constituent images are inputs to a deferred rendering algorithm which allows the user to control which objects are in view and how each is colored. 

A Cinema database holds a collection of results sampled by a set of visualization parameters. Visualization parameters can be created for any control that the user might have in a traditional visualization session. Examples include isosurfaces, slice planes and viewpoint. Cinema viewing applications give the user a control for each parameter setting and display the corresponding precomputed results, as if the user had made the same choices in a traditional visualization tool from the full resolution input data. As cinema holds only images, it is much more lightweight and display is always a constant time operation regardless of the parameter that is being changed.

Examples of typical parameters in a cinema database are:
\begin{itemize}
\item \textbf{Time}. Time varying data can be sampled at arbitrary points along the temporal domain.
\item \textbf{Camera positions}. In a static camera the position and orientation is fixed. In a spherical camera the position varies over a set of positions centered around a chosen focal point. More complicated camera tracks are possible.
\item Zero or more \textbf{operators}, such as clipping plane and isocontour samples along their respective ranges. In \chaplin Cinema Databases each result is sampled and saved in isolation from all others with nothing else visible in the scene. In a viewing application, the user can choose any number of these samples and see them rendered together with correct occlusion culling.
\item \textbf{Color} components as described next.
\end{itemize}

In a Cinema workflow, the application producing a Cinema Database creates a set of image constituents for each sample in the parameter space. Technically it does so by iterating over all assigned values for the color parameter of the currently displayed object and capturing each image. Likewise viewing applications let the user choose an object, take in the set of color results for that object and use them to draw that object on the screen. Presently, the image constituents can be:
\begin{itemize}
\item \textbf{Color} image. This encodes a standard RGB value for each pixel - the result of rendering the viewpoint from the camera. The earlier \textit{Simple} specification was limited to these. The new capabilities of the \chaplin specification require that each color image be limited to showing just only one object from the scene at a time.
\item \textbf{Lookup Table} image. This is another name for a standard RGB image. Use of this label indicates only that the RGB raster was originally produced by applying a colormap to a specific object's data values.
\item \textbf{Depth} image. This encodes a depth value for every pixel, relative to the camera. This is a requirement for compositing.
\item \textbf{Value} image. This encodes an arbitrary array value associated with the data that is visualized at each pixel. This is a requirement for dynamic colormapping in the viewer.
\item \textbf{Luminance} image. This encodes a rendered shading brightness for each pixel. This is required for lighting of dynamic colormapped objects, as otherwise they would appear unlit with no 3D shape queues.
\end{itemize}
It is important to note that not all image constituents must be present. Because Cinema viewing applications combine these components together to produce pictures on demand for the user, the final result is dependent upon what image constituents are available and what how the user has decided to see the result (creating different color maps for example).

In addition to the sampled data, a cinema database contains metadata that describes what the samples are, how they are stored and how they can be used. It must:
\begin{itemize}
\item identify the specific format (version number and content type) of the Cinema database,
\item enumerate the entire set of parameters that are explorable in the database,
\item map parameter value combinations to storage locations,
\item describe relationships between parameters so that generators can produce the full set without skipping valid items or visiting impossible combinations and so the viewers can choose from within that consistent set,
\item annotate parameters as necessary to indicate how they need to be handled
  \begin{itemize}
    \item define overall classes of parameters
    \item define the type (depth image, color image, value rendered image, ...) of each image stored in the database,
    \item define the storage format (jpeg encoded buffer, png encoded buffer, raw float binary buffer, text file, etc...) of each image stored in the database,
  \end{itemize}
\end{itemize}
